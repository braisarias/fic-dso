#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>


#define MAX_FOR 4
#define MAX_CHAR 5
#define MAX 50
#define MAX_STACK 100
#define FILENAME_OUT "provas.txt"

// stack indexes
#define I_NEXT_FRAME 8
#define I_RET_DIR 10

// size of frames
#define TAM_FRAME_YIELD 12
#define TAM_FRAME_YIELD_ 12
#define TAM_FRAME1 16
#define TAM_FRAME2 16


char t1First, t2First;
int *stackt1, *stackt2;
int pc1, pc2; // para saber a onde hai que saltar
FILE *fOut; // imprimir info pila
char activeThread;
unsigned char yieldCount=0;
char verbose=0;

/*
ssize_t write(int fd, const void *buf, size_t count);
int fprintf(FILE *stream, const char *format, ...);
FILE *fopen(const char *path, const char *mode);
*/

void _yield(){
	int a[MAX_CHAR] = {111,111,111,111,111};
	int i;
	
	if (verbose){
		// imprimir a pila a ficheiro
		for (i=0; i<MAX; i++){
			fprintf(fOut, "%3d: [0x%x] %13d\t0x%x\n", i, &(a[i]), a[i], a[i]);
		}
	}
	
	
	
	// if esta en t1
		// cambiar frame anterior a t2
		// cambiar dir retorno á dir de t2
	//if esta en t2
		// cambiar frame anterior a t1
		// cambiar dir retorno á dir de t1
		
	if (activeThread == 1){
		// estamos en T1, temos que cambiar a T2
		
		// copiamos dir de retorno para saber onde vai o Thread 1
		pc1 = a[TAM_FRAME_YIELD + I_RET_DIR];
		
		// copiar o contido da frame de T1 para o heap
		memcpy(stackt1, &(a[TAM_FRAME_YIELD+TAM_FRAME_YIELD_]), TAM_FRAME1-1);
		
		if (!t1First){
			// copiar o contido do stack de T2, para machacar o de T1
			memcpy(&(a[TAM_FRAME_YIELD+TAM_FRAME_YIELD_]), stackt2, TAM_FRAME2-1);
			
			// cambiar a dir de retorno
			a[TAM_FRAME_YIELD + I_RET_DIR] = pc2;
			
			// cambiar o punteiro ao frame anterior
				// non sei se fará falta
		} else{
			// cambiar dir retorno a dir retorno frame de T1 (main) NON
			// ten que volver ao yield
			//a[I_RET_DIR] = a[TAM_FRAME_YIELD+I_RET_DIR]; //12+10=22
			
			// cambiar dir frame anterior á frame do main
			a[I_NEXT_FRAME] = a[TAM_FRAME_YIELD + I_NEXT_FRAME];
			
			// cambiar a dir de retorno do yield para a de T1 (no codigo de main)
			a[TAM_FRAME_YIELD + I_RET_DIR] = a[TAM_FRAME_YIELD + TAM_FRAME_YIELD_ + I_RET_DIR];
			
			t1First = 0;
		}
	} else{
		if (activeThread == 2){
			// estamos en T2, temos que cambiar a T1
			
			// copiamos dir de retorno para saber onde vai o Thread 2
			pc2 = a[TAM_FRAME_YIELD + I_RET_DIR];
			
			if(t2First){
				// copiar o contido da frame de T2 para o heap
				memcpy(stackt2, &(a[TAM_FRAME_YIELD + TAM_FRAME_YIELD_]), TAM_FRAME2-1);
				
				// cambiar dir frame anterior á frame do main
// 				a[I_NEXT_FRAME] = a[TAM_FRAME_YIELD+I_NEXT_FRAME];
				
				t2First = 0;
			}

			// copiar o contido do stack de T1, para machacar o de T2
			memcpy(&(a[TAM_FRAME_YIELD + TAM_FRAME_YIELD_]), stackt1, TAM_FRAME1-1);
			
			// cambiar a dir de retorno
			a[TAM_FRAME_YIELD + I_RET_DIR] = pc1;
			
			
		
		} else{
			// isto non debera executarse nunca
			write(1, "PANIC\n", 6);
			exit(-1);
		}
	}
	
	
	if (verbose){
		fprintf(fOut, "-------------\n");
		
		for (i=0; i<MAX; i++){
			fprintf(fOut, "%3d: [0x%x] %13d\t0x%x\n", i, &(a[i]), a[i], a[i]);
		}
		fprintf(fOut, "**************************************\n");
	}
	
	yieldCount++;
	return;
}

void yield(){
	int a[MAX_CHAR]={222,222,222,222,222};
	_yield();
}


int t1(){
	int a[MAX_CHAR]={101,101,101,101,101};
	int i;
	
	for(i=0; ; i++){
		if (verbose){
			(fOut, "%i\n", i);
		}
		write(1, "A\n", 2);
		activeThread = 1;
		
		yield();
		
	}
	return 0;
}




int t2(){
	int a[MAX_CHAR]={202,202,202,202,202};
	int i;
	
	
	for(i=0; ; i++){
		if (verbose){
			(fOut, "%i\n", i);
		}
		write(1, "B\n", 2);
		activeThread = 2;
		
		yield();
	}
	
	return 0;
}



int main(int argc, char **argv){
	int a[MAX_CHAR]={1010,1010,1010,1010,1010};
	
	if ((stackt1 = malloc(MAX_STACK*sizeof(int))) == NULL){
		write(1, "Error: malloc\n", 14);
		return -1;
	}
	
	if ((stackt2 = malloc(MAX_STACK*sizeof(int))) == NULL){
		write(1, "Error: malloc\n", 14);
		return -1;
	}
	
	if (argc > 1){
		if (strcmp(argv[1], "-v") == 0){
			verbose=1;
		}
	}
	
	if (verbose){
		fOut = fopen(FILENAME_OUT, "w");
		if (fOut == NULL){
			write(1, "Error: fopen\n", 13);
			return -2;
		}
	}
	
	t1First = t2First = 1;
	
	
	t1();
	if (verbose){
			(fOut, "=========================================\n");
	}
	t2();


	if (verbose){
		(fOut, "yieldCount %d\n", yieldCount);
		fclose(fOut);
	}
	free(stackt1);
	free(stackt2);
	return 0;
}

