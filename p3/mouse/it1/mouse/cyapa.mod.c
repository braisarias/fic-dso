#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xb882ea52, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x39a28b00, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0xa855010d, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0x99e66c5e, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0x54e4bf78, __VMLINUX_SYMBOL_STR(input_free_device) },
	{ 0x1ec299c1, __VMLINUX_SYMBOL_STR(input_mt_init_slots) },
	{ 0x486f4827, __VMLINUX_SYMBOL_STR(input_alloc_absinfo) },
	{ 0x4ff90e2c, __VMLINUX_SYMBOL_STR(input_set_abs_params) },
	{ 0x160ed351, __VMLINUX_SYMBOL_STR(input_allocate_device) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0xe7c4b0c8, __VMLINUX_SYMBOL_STR(i2c_smbus_write_i2c_block_data) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x3ca68c1, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0xa315f0c2, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x6f53ca99, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x32211299, __VMLINUX_SYMBOL_STR(input_mt_sync_frame) },
	{ 0x69a62efd, __VMLINUX_SYMBOL_STR(input_mt_report_slot_state) },
	{ 0x91314f22, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0x69f0bfcd, __VMLINUX_SYMBOL_STR(pm_wakeup_event) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xd13c5f61, __VMLINUX_SYMBOL_STR(i2c_smbus_read_block_data) },
	{ 0xc5b95c7d, __VMLINUX_SYMBOL_STR(i2c_smbus_read_i2c_block_data) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x651aad74, __VMLINUX_SYMBOL_STR(input_unregister_device) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0x3ce4ca6f, __VMLINUX_SYMBOL_STR(disable_irq) },
	{ 0xce2840e7, __VMLINUX_SYMBOL_STR(irq_set_irq_wake) },
	{ 0xfcec0987, __VMLINUX_SYMBOL_STR(enable_irq) },
	{ 0x3a358e87, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x611cbf7, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x7dbba898, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x9b1eaeac, __VMLINUX_SYMBOL_STR(i2c_smbus_write_byte_data) },
	{ 0x35232da2, __VMLINUX_SYMBOL_STR(i2c_smbus_read_byte_data) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=i2c-core";

MODULE_ALIAS("i2c:cyapa");
