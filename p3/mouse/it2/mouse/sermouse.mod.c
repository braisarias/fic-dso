#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xb882ea52, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xd47f8cdd, __VMLINUX_SYMBOL_STR(serio_unregister_driver) },
	{ 0xbe8e1c93, __VMLINUX_SYMBOL_STR(__serio_register_driver) },
	{ 0x99e66c5e, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0x7cb6c5ec, __VMLINUX_SYMBOL_STR(serio_open) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x54e4bf78, __VMLINUX_SYMBOL_STR(input_free_device) },
	{ 0x160ed351, __VMLINUX_SYMBOL_STR(input_allocate_device) },
	{ 0xa315f0c2, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x6f53ca99, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x91314f22, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x651aad74, __VMLINUX_SYMBOL_STR(input_unregister_device) },
	{ 0x3ca68c1, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x640bbec0, __VMLINUX_SYMBOL_STR(serio_close) },
	{ 0x611cbf7, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("serio:ty02pr01id*ex*");
MODULE_ALIAS("serio:ty02pr02id*ex*");
MODULE_ALIAS("serio:ty02pr03id*ex*");
MODULE_ALIAS("serio:ty02pr04id*ex*");
MODULE_ALIAS("serio:ty02pr05id*ex*");
MODULE_ALIAS("serio:ty02pr06id*ex*");
MODULE_ALIAS("serio:ty02pr07id*ex*");
