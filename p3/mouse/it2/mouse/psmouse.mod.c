#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xb882ea52, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x57d9a9da, __VMLINUX_SYMBOL_STR(device_remove_file) },
	{ 0x6f53ca99, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xc996d097, __VMLINUX_SYMBOL_STR(del_timer) },
	{ 0x754d539c, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x3ca68c1, __VMLINUX_SYMBOL_STR(dev_set_drvdata) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xc8b57c27, __VMLINUX_SYMBOL_STR(autoremove_wake_function) },
	{ 0x49cab779, __VMLINUX_SYMBOL_STR(dev_printk) },
	{ 0xd47f8cdd, __VMLINUX_SYMBOL_STR(serio_unregister_driver) },
	{ 0x6cb13d57, __VMLINUX_SYMBOL_STR(ps2_handle_ack) },
	{ 0x2f66ba6, __VMLINUX_SYMBOL_STR(ps2_sendbyte) },
	{ 0x77191bdb, __VMLINUX_SYMBOL_STR(ps2_handle_response) },
	{ 0x6288506b, __VMLINUX_SYMBOL_STR(input_mt_report_finger_count) },
	{ 0x20000329, __VMLINUX_SYMBOL_STR(simple_strtoul) },
	{ 0x486f4827, __VMLINUX_SYMBOL_STR(input_alloc_absinfo) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0xc1306d8a, __VMLINUX_SYMBOL_STR(ps2_end_command) },
	{ 0x1f07345d, __VMLINUX_SYMBOL_STR(__ps2_command) },
	{ 0x33ba5cd4, __VMLINUX_SYMBOL_STR(param_ops_bool) },
	{ 0x593a99b, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0xbc9b3413, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x8df16b85, __VMLINUX_SYMBOL_STR(serio_interrupt) },
	{ 0xd597cb5, __VMLINUX_SYMBOL_STR(ps2_drain) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xc28a4894, __VMLINUX_SYMBOL_STR(sysfs_remove_group) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x3eb70226, __VMLINUX_SYMBOL_STR(input_mt_report_pointer_emulation) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x4ff90e2c, __VMLINUX_SYMBOL_STR(input_set_abs_params) },
	{ 0x91314f22, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0xd2b042c8, __VMLINUX_SYMBOL_STR(serio_unregister_child_port) },
	{ 0xd5f2172f, __VMLINUX_SYMBOL_STR(del_timer_sync) },
	{ 0xd4835ef8, __VMLINUX_SYMBOL_STR(dmi_check_system) },
	{ 0xfb578fc5, __VMLINUX_SYMBOL_STR(memset) },
	{ 0xbe8e1c93, __VMLINUX_SYMBOL_STR(__serio_register_driver) },
	{ 0x5a5a94a6, __VMLINUX_SYMBOL_STR(kstrtou8) },
	{ 0x7dbba898, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x9a370f47, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0xf201f69d, __VMLINUX_SYMBOL_STR(mutex_lock_interruptible) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xdebee7c8, __VMLINUX_SYMBOL_STR(sysfs_create_group) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x16305289, __VMLINUX_SYMBOL_STR(warn_slowpath_null) },
	{ 0xd66120b4, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x8834396c, __VMLINUX_SYMBOL_STR(mod_timer) },
	{ 0x1ec299c1, __VMLINUX_SYMBOL_STR(input_mt_init_slots) },
	{ 0x373db350, __VMLINUX_SYMBOL_STR(kstrtoint) },
	{ 0x640bbec0, __VMLINUX_SYMBOL_STR(serio_close) },
	{ 0x7cb6c5ec, __VMLINUX_SYMBOL_STR(serio_open) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x40f33fcb, __VMLINUX_SYMBOL_STR(device_create_file) },
	{ 0x4ca22c02, __VMLINUX_SYMBOL_STR(dev_notice) },
	{ 0x4d751d5f, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x78764f4e, __VMLINUX_SYMBOL_STR(pv_irq_ops) },
	{ 0xa4e10716, __VMLINUX_SYMBOL_STR(ps2_command) },
	{ 0xeeec26a7, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x99e66c5e, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0xd62c833f, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x43261dca, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irq) },
	{ 0x54e4bf78, __VMLINUX_SYMBOL_STR(input_free_device) },
	{ 0x999eab10, __VMLINUX_SYMBOL_STR(ps2_init) },
	{ 0xa315f0c2, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x82592121, __VMLINUX_SYMBOL_STR(serio_reconnect) },
	{ 0x35cede20, __VMLINUX_SYMBOL_STR(__serio_register_port) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xa46f2f1b, __VMLINUX_SYMBOL_STR(kstrtouint) },
	{ 0x69acdf38, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x651aad74, __VMLINUX_SYMBOL_STR(input_unregister_device) },
	{ 0x5c8b5ce8, __VMLINUX_SYMBOL_STR(prepare_to_wait) },
	{ 0x69a62efd, __VMLINUX_SYMBOL_STR(input_mt_report_slot_state) },
	{ 0x975e4000, __VMLINUX_SYMBOL_STR(ps2_cmd_aborted) },
	{ 0xfa66f77c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x32211299, __VMLINUX_SYMBOL_STR(input_mt_sync_frame) },
	{ 0x3a358e87, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x47c8baf4, __VMLINUX_SYMBOL_STR(param_ops_uint) },
	{ 0x611cbf7, __VMLINUX_SYMBOL_STR(dev_get_drvdata) },
	{ 0x52279d03, __VMLINUX_SYMBOL_STR(ps2_begin_command) },
	{ 0x4978cec8, __VMLINUX_SYMBOL_STR(input_mt_assign_slots) },
	{ 0x160ed351, __VMLINUX_SYMBOL_STR(input_allocate_device) },
	{ 0x81e6b37f, __VMLINUX_SYMBOL_STR(dmi_get_system_info) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("serio:ty01pr*id*ex*");
MODULE_ALIAS("serio:ty05pr*id*ex*");
