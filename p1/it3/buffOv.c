#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define MAX 50
#define MAXCHAR 10
#define SALTO 10
#define DIR_RET 14
#define DIR_NEXT_FRAME 12
#define SALT_FRAME 80

#define F_STACK "stack.dat"
#define F_OUT "buffOv.log"

FILE *fLog;
char *p;


int f2(int n){
	printf("f2\n\n");
	if (n != 0){
		f2(n-1);
	} else {
		printf("n: %d - n+n: %d\n", n, n+n+1);
	}
	return 0;
}


int f(int n){
	int i, fd;
// 	int cagarruta;
	int a[MAXCHAR] = {111, 111, 111, 111, 111, 111, 111, 111, 111, 111};
	i = 0xFF;
	char input[4]={'A','a','A','a'};
	
	p = input;
	
	if ((fd = open(F_STACK, O_WRONLY | O_CREAT, 0700)) == -1){
		perror("Erro abrindo ficheiro");
		exit(-1);
	}

	if (write(fd, input, MAX*(sizeof(int))) == -1){
		perror("Erro escribindo ficheiro");
		exit(-1);
	}
	close(fd);

	fprintf(fLog, "antes scanf\n\n");
	for (i=-10; i< MAX; i++){
		fprintf(fLog, "%3d f1 [0x%x] -> %d : 0x%x\n", i, &(a[i]), a[i], a[i]);
	}
// 	scanf
	scanf("%s", input);
	
	fprintf(fLog, "funcion\n\n");
	for (i=-10; i< MAX; i++){
		fprintf(fLog, "%3d f1 [0x%x] -> %d : 0x%x\n", i, &(a[i]), a[i], a[i]);
	}
// 	f2(5);
	
	return 0;
}


int main(int argc, char *argv[]){
	int a[MAXCHAR] = {1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010, 1010};
	
	fLog = fopen(F_OUT, "w");
	if (fLog == NULL){
		perror("Erro abrindo ficheiro");
		exit(-1);
	}
	
	// creamos ficheiro dump do stack
	printf("print stack\n");
	f(5);
	printf("caca\n");
	f2(5);
	printf("ultimo\n");
	
	
	
	
	return 0;
}
