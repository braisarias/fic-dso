#include <stdio.h>

#define MAX 100
#define MAXCHAR 10
#define SALTO 10
#define DIR_RET 14

int f2(){
	int i;
	int a[MAXCHAR];
	
	printf("f2\n");
	
	for (i=-1; i< MAX; i++){
			printf("f2 %3d: [0x%x] -> %d : 0x%x\n", i, &(a[i]), a[i], a[i]);
		}
	return 0;
}

int f(int n, int n1, int n2, int n3){
	int i;
	int a[MAXCHAR];
	
	for(i=0; i<MAXCHAR; i++){
		a[i] = 101;
	}
	
	if (n != 0){
		f(n-1, n1-1, n2-1, n3-1);
		printf("1");
		printf("2");
	} else {
		
		for (i=-1; i< MAX; i++){
			if (i==DIR_RET){
				printf("-->");
			}
			printf("f2 %3d: [0x%x] -> %d : 0x%x\n", i, &(a[i]), a[i], a[i]);
			//printf("----------------n: %d\n", n);
		}
		printf("\na[%d] = 0x%x\n", DIR_RET, a[DIR_RET]);
		printf("\na[%d] + %d = 0x%x\n", DIR_RET, SALTO, a[DIR_RET]+SALTO);
// 		a[DIR_RET] += SALTO;
		
		f2();
	}
	return 0;
}


int main(){
	f(5,5,5,5);
	return 0;
}
